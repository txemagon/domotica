# Funciones Lógicas

Tienen la forma:

```math
 f = a \cdot \overline{b} \cdot c + \overline{a} \cdot b + \overline{ \overline{a} \cdot \overline{b} }
```

## Funciones y Tablas de Verdad

Nos vamos a preguntar en qué casos la función vale 1 o en qué casos la función vale 0.

Por ejemplo:

```math
 f = \overline{a}b + c
```

| a b c | f |
| ----- | - |
| 0 0 0 | 0 | 
| 0 0 1 | 1 |
| 0 1 0 | 1 |
| 0 1 1 | 1 |
| 1 0 0 | 0 |
| 1 0 1 | 1 |
| 1 1 0 | 0 |
| 1 1 1 | 1 |

```math
 f = a + \overline{b} \overline{c}
```

| a b c | f |
| ----- | - |
| 0 0 0 | 1 | 
| 0 0 1 | 0 |
| 0 1 0 | 0 |
| 0 1 1 | 0 |
| 1 0 0 | 1 |
| 1 0 1 | 1 |
| 1 1 0 | 1 |
| 1 1 1 | 1 |

## Funciones canónicas

Cuando a un término le falta una variable, representa más de un 1 en la tabla de verdad.
Cuando no falta ninguna variable en ningún término se dice que la función es canónica.

Esto no es canónico,
```math
f = a + \overline{b}c
```

pero se puede ir haciendo canónico desdoblando los casos:

```math
f = a + a\overline{b}c + \overline{a}\overline{b}c
```

### Forma de suma de productos

```math
f = a \overline{b} c + a \overline{b} \overline{c} + \overline{a} b \overline{c}
```

En las funciones canónicas en forma de Suma De Productos buscamos los 1's de la función.


| a b c | f |
| ----- | - |
| 0 0 0 | 0 | 
| 0 0 1 | 0 |
| 0 1 0 | 1 |
| 0 1 1 | 0 |
| 1 0 0 | 1 |
| 1 0 1 | 1 |
| 1 1 0 | 0 |
| 1 1 1 | 0 |


### Forma de Producto De Sumas

```math
f = (a + b + c) \cdot (\overline{a} + b + \overline{c}) \cdot (a + \overline{b} + c)
```

Lo importante es buscar cuando la función vale 0.

En el caso anterior: (000, 101, 010)


| a b c | f |
| ----- | - |
| 0 0 0 | 0 | 
| 0 0 1 | 1 |
| 0 1 0 | 0 |
| 0 1 1 | 1 |
| 1 0 0 | 1 |
| 1 0 1 | 0 |
| 1 1 0 | 1 |
| 1 1 1 | 1 |

## Mapas de Karnaugh

Reglas:

1. Cada grupo debe de tener la mayor cantidad de 1's posible.
1. Menor número de grupos.
1. Los grupos deben contener 2, 4, 8, 16 1's (potencia de 2)