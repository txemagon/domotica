# Operaciones

## Operaciones Aritméticas

### Suma

En decimal

```
 (1)
  47
+ 36
----
  83 
```

6 mas 7 no decimos que sean 13, decimos que son 3 y que hemos hecho (1) paquete de 10.

Igualmente en otras bases, como por ejemplo el hexadecimal:

```
   (1)
    5Ah
  + 68h
-------
    C2h
```

decimos que A y 8 son 2 y que hacemos (1) paquete de 16.

### Resta

Resta no. Suma de números negativos.

¿Qué es un negativo?
Un número negativo es aquél que sumado al positivo correspondiente da 0.

¿Cómo lo vamos a conseguir?
Aprovechando que el númro de bits (cables o lo que sea) es finito. Para ello vamos a usar el Ca2 (complemento a 2).

Hay que elegir cuantos bits queremos usar.

```
MyS(1+4)
+---+---+---+---+---+
|   |   |   |   |   |
+---+---+---+---+---+
```
El número de cables limita la cantidad de bits que puedes poner.

Para poner el número 3, habitualmente lo escribimos así:
```
(3)
+---+---+---+---+---+
|   |   |   | 1 | 1 |
+---+---+---+---+---+
```

A nivel físico los cables que esten por delante de los unos tienen que estar a 0.

```
+---+---+---+---+---+
| 0 | 0 | 0 | 1 | 1 |
+---+---+---+---+---+
```
Por lo tanto el primer bit, que yo he dicho que iba a indicar el signo, vale 0 cuando el número es positivo.

Eso se llama convenio de Magnitud y Signo e indica que el primer bit es el de signo.

```
MyS(1+4)
+---+---+---+---+---+
|   |   |   |   |   |
+---+---+---+---+---+
  ^
  |
  +------ Bit de Signo
```

Entonces encontrar un 1 en esta posición nos va a indicar que el número es negativo.

Pero, no podemos hacer el -3 como un 3 con un 1 delante, porque si no al sumarlos no daría 0.

```
MyS(1+4)
               1   1
     +---+---+---+---+---+
     | 0 | 0 | 0 | 1 | 1 |   (3)
     +---+---+---+---+---+

     +---+---+---+---+---+
 +   | 1 | 0 | 0 | 1 | 1 |  (-3) mal hecho
     +---+---+---+---+---+
___________________________

     +---+---+---+---+---+
     | 1 | 0 | 1 | 1 | 0 |  (-6)
     +---+---+---+---+---+

```

Según nuestra estúpida teoría -3 + 3 = -6

Necesitamos encontrar una representación del -3 que sumada a 3 dé 0.

Esa representación es el Ca2 que consiste en hacer el Ca1 (cambiar 1 y 0's) y sumarle 1.

```
MyS(1+4)
+---+---+---+---+---+
| 0 | 0 | 0 | 1 | 1 | (3)
+---+---+---+---+---+

+---+---+---+---+---+
| 1 | 1 | 1 | 0 | 0 | Ca1(3)
+---+---+---+---+---+

+---+---+---+---+---+
| 1 | 1 | 1 | 0 | 1 | Ca2(3) => Buena representación de -3
+---+---+---+---+---+

```

¿-3 + 3 == 0?

```
MyS(1+4)
   1   1   1   1   1
     +---+---+---+---+---+
     | 0 | 0 | 0 | 1 | 1 |   (3)
     +---+---+---+---+---+

     +---+---+---+---+---+
 +   | 1 | 1 | 1 | 0 | 1 |  (-3) bien hecho
     +---+---+---+---+---+
___________________________

     +---+---+---+---+---+
   1 | 0 | 0 | 0 | 0 | 0 |  (0)
     +---+---+---+---+---+
   ^
   |
   +---- Who cares? It even doesn't physically exist.
```

¡Yay!

Por ejemplo 7 - 4

```
    111
     00111 ( 7)
   + 11100 (-4)
  ______________
   1|00011 ( 3)
   ^
   |
   +---- Este 1 no se ve. Se ha desbordado.
```

Ca2 rápido.

Hagamos paso a paso el Ca2 del 4

```
     00100 (4)
     11011 Ca1(4)
     11100 Ca2(4) => (-4)
```

Para hacer el Ca2 de manera rápida podemos dejar intacto desde el último 1 por la derecha.
Y de ahí hacia la izquierda cambiar los 0's por 1's.

Otro ejemplo -7+4

```
  11001 (-7)
+ 00100 ( 4)
____________
  11101 (?) Es negativo
  00011 Ca2(?) = 3 => El número misterioso es (-3)
```

### Multiplicación

#### Multiplicación Binaria

```
     1101 (13)
    x 101 ( 5)
  ________
     1101 (13 x 1)
    0000  (26 x 0)
   1101   (52 x 1)
_________
  1000001  (65 = 64 + 1)
```

Multiplicación a la Russe

```
5   13   <-
2   26
1   52   <-
_______
    65
```

#### Multiplicar es desplazar los bits hacia la derecha.


Multiplicar 10 desplaza los números hacia la izquierda

```math
97 \times 10 = 970

```
```math
970 \times 10 = 9700
```

¿Por qué se desplaza?


```math
97 \times 10 = (9\cdot10^1+7\cdot10^0)\times 10
```
```math
97 \times 10 = (9\cdot10^1\cdot10+7\cdot10^0\cdot10)
```
```math
97 \times 10 = 9\cdot10^{1+1}+7\cdot10^{0+1}
```
```math
97 \times 10 = 9\cdot10^2+7\cdot10^1 = 970
```

Al multiplicar por 2 en binario también se desplazan los dígitos.

```math
9 \times 2 = 1001 \times 10
```

```math
9 \times 2 = (1 \cdot 2^3 + 0 \cdot 2^2 + 0 \cdot 2^1 + 1 \cdot 2^0) \times 2
```
```math
9 \times 2 = 1 \cdot 2^4 + 0 \cdot 2^3 + 0 \cdot 2^2 + 1 \cdot 2^1 = 10010 = 18
```


### División

Dividir es llevar los bits hacia la derecha.

## Operaciones Lógicas

Se basan en dos valores no numéricos: `true` y `false`.  
Se especifican en función de una tabla que da el resultado de la operación para cada uno de los posibles valores de entrada. A esta tabla se le llama **tabla de verdad**.

### AND

| a b | AND |
| --- |:---:|
| 0 0 |  0  |
| 0 1 |  0  |
| 1 0 |  0  |
| 1 1 |  1  |

```math
c = a \cdot b
```


### OR

| a b | OR  |
| --- |:---:|
| 0 0 |  0  |
| 0 1 |  1  |
| 1 0 |  1  |
| 1 1 |  1  |


```math
c = a + b
```

### XOR

| a b | XOR |
| --- |:---:|
| 0 0 |  0  |
| 0 1 |  1  |
| 1 0 |  1  |
| 1 1 |  0  |


```math
c = a \oplus b
```

### NOT

| a | NOT |
|---|:---:|
| 0 |  1  |
| 1 |  0  |


```math
c = \overline{a}
```

### NAND

| a b | NAND |
| --- |:---:|
| 0 0 |  1  |
| 0 1 |  1  |
| 1 0 |  1  |
| 1 1 |  0  |

```math
c = \overline{a \cdot b}
```

### NOR

| a b | NOR |
| --- |:---:|
| 0 0 |  1  |
| 0 1 |  0  |
| 1 0 |  0  |
| 1 1 |  0  |

```math
c = \overline{a + b}
```

### XNOR

| a b | XNOR |
| --- |:---:|
| 0 0 |  1  |
| 0 1 |  0  |
| 1 0 |  0  |
| 1 1 |  1  |

```math
c = \overline{a \oplus b}
```


### Símbolos eléctricos

![Símbolos eléctricos](https://dryuc24b85zbr.cloudfront.net/tes/resources/11139006/image?width=500&height=500&version=1519313233302)

### Leyes de DeMorgan

Si no se tienen que cumplir `a` y `b` a la vez, basta con que o no se cumpla `a` o no se cumple `b`.
```math
\overline{a \cdot b} = \overline{a} + \overline{b} 
```

Si no puede pasar `a` o `b` es lo mismo que que no pasen ni `a` ni `b`.
```math
\overline{a + b} = \overline{a} \cdot \overline{b} 
```

## Propiedades Fundamentales

### AND

```math
a \cdot 0 = 0
```

```math
a \cdot 1 = a
```


```math
a \cdot a = a
```


```math
a \cdot \overline{a} = 0
```

### OR

```math
 a + 0 = a
```

```math
 a + 1 = 1
```


```math
 a + a = a
```


```math
 a + \overline{a} = 1
```

### XOR

```math
a \oplus 0 = a
```


```math
a \oplus 1 = \overline{a}
```


```math
a \oplus a = 0
```


```math
a \oplus \overline{a} = 1
```