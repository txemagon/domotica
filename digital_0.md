# TEMA 0. ELECTRONICA DIGITAL

## Sistemas Numéricos

### Cambios de Base

Cambiar de base es como hacer paquetes con las unidades sueltas.
Pasar de una base a decimal es como extraer las unidades desde los paquetes.

Algunos ejemplos:

#### Base 20

```
 PC    MD
 12 => 22
 17 => 27
 2(10) => 50
 43 => 83
 10 => 20
 44 => 84
```

#### Base 16

```
 19 => 25
 37 => 55
 25 => 37
 44 => 68
 2(10) => 42
 2A => 42
```

Cuando los paquetes pueden tener más de 10 unidades, necesito poner letras.

```
  0 => 0
  1 => 1
  2 => 2
  3 => 3
  .
  . 
  .
  9 => 9
 10 => A
 11 => B
 12 => C
 13 => D
 14 => E
 15 => F
 16 => 10
```


#### Base 8

```
25 => 21
44 => 36
29 => 31 => 25
```

### Base 2

```
8421
   0 -> 0
   1 -> 1
  10 -> 2
  11 -> 3
 100 -> 4
 101 -> 5
 110 -> 6
 111 -> 7
1000 -> 8
1001 -> 9
1010 ->10
```

Algunos ejemplos:

```
    1
    2631
    8426 8421
 20    1 0100
210 1101 0010
 50 0011 0010
  9 0000 1001
 21 0001 0101
 77 0100 1101
 88 0101 1000
100 0110 0100
 13      1101
 31    1 1111
 32   10 0000
 69  100 0101
 27    1 1011
 35   10 0011   
```
## SUMAS

### Base 16

Vamos a ir sumando de uno en uno: 
1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,10

Empezando por 7C 
7C,7D,7E,7F,80

O por FF  
FF,100  

100 Es un cartón con 0 paquetes y 0 sueltos   
16x16 => 16x10 + 16x6 = 160 + 96 = 256  

Y FF uno menos. 
FF + 1 = 100 => 256  



## El epígrafe misterioso

Un cambio a todas las bases
```
137 => (17)1 = 211 (8)
137 => 89 (16)
137 => 1000 1001
```

Y curiosamente convirtiendo el binario por grupos separados
```
10 001 001
2   1  1

1000 1001
  8    9
```

Un sprite:

183C7EDBFF5A8142

```
   00   
  0000  
```